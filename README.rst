XFCE4
=====

This repository holds the build sequence and scripts for the xfce4 desktop
on different vectorlinux releases.  The changes from one release to the next are
tracked on different branches of this repository.

Master branch should remain empty. 

The bot will update to 'veclinux-X.X' upon pull.

src/xfce4.SlackBuild should trigger the build of all packages
